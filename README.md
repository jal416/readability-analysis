# Dale Chall #

Dale-Chall is a measure of Readability. This repo is a java implementation of it.


## Compiling ##
Build the java code:

```
#!bash

javac Prog3.java Text.java DaleChall.java
```
And run it:


```
#!bash

java Prog3 Green_Eggs_and_Ham.txt Dale-ChallWordList.txt
```
