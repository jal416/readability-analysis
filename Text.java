/*
 * author: Jack Lamberti
 * Version 1.0
 * Created: 1/31/2013
 * Last Modified: Never
 */

public class Text {

    private String str;
    private int wordCount = 0;
    private int sentenceCount = 0;
    private String[] words;

    public Text(String str) {
        this.str = str;
        calculateStats();
    }

    public int getWordCount() {
        return wordCount;
    }

    public int getSentenceCount() {
        return sentenceCount;
    }

    public String[] getWords() {
        return words;
    }

    private void calculateStats() {
        //Deciced against checking various ascii whitespace characters in case 
        //Java's regex engine cannot catch all of them with \\s 
        //(\\s | \\u0009 | \\u000A | \\u000B | \\ u000C | \\u000D | \\u0020 | \\u0085 | \\u00A0 | \\u2028 | \\u2029)+
        words = str.split("(\\s)+");
        wordCount = words.length;
        for (int i = 0; i < words.length; i++) {
            try {
                if (words[i].substring(words[i].length() - 1).matches("[, : ;]+")) {
                    words[i] = words[i].substring(0, words[i].length() - 1);
                } else if (words[i].substring(words[i].length() - 1).matches("[! \\? \\.]+")) {
                    words[i] = words[i].substring(0, words[i].length() - 1);
                    ++sentenceCount;
                }
            } catch (Exception e) {
                //Catch Strings that aren't words
                //In Romeo and Juliet, a strange ascii characters made there way into the words array
                //This is just a preventive try catch since I couldn't find the root of the issue... - jal416 on 1/31/13
                --wordCount;
            }
        }
    }
}
