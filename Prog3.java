/* 
 * James Lamberti
 * Program Description: This program computes the readability of a text
 */

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class Prog3 {
    private final static String EOL = System.lineSeparator();
    public static void main(String[] args) {
        if (args.length != 2) {
            System.out.println("Must enter two arguments" + EOL + "Usage: <Text> <Dale-Chall File>");
            System.exit(0);
        } else {
            StringBuilder sb = new StringBuilder();
            File f = new File(args[0]);
            if (f.exists()) {   // If file exists, read file
                FileInputStream fistream = null;
                try {
                    fistream = new FileInputStream(args[0]);
                    try (DataInputStream in = new DataInputStream(fistream)) {
                        BufferedReader br = new BufferedReader(new InputStreamReader(in));
                        String line;
                        while ((line = br.readLine()) != null) {
                            sb.append(line);
                            sb.append(" ");
                        }
                    }
                } catch (IOException ex) {
                    System.out.println("Error when reading file..." + EOL + "Program will now exit");
                    System.exit(0);
                } finally {
                    try {
                        fistream.close();
                    } catch (IOException ex) {  //Just in case...
                        System.out.println("Error when reading file..." + EOL + "Program will now exit");
                        System.exit(0);
                    }
                }
            } else {    //File doesn't exist...
                System.out.println("File containing the text to be analyzed does not exist!" + EOL + "Program will now exit");
                System.exit(0);
            }
            Text text = new Text(sb.toString());
            DaleChall dChall = new DaleChall(text, args[1]);
            dChall.printGradeLevel();
        }
    }
}
