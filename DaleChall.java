import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.Arrays;

/*
 * author: Jack Lamberti
 * Version 1.1
 * Created: 1/31/2013
 * Last Modified: 2/1/2013
 * Changes:
 *  -Added rounding to the raw Dale-Chall Number
 * Requirements:
 *  -Text.java
 */
public class DaleChall {

    private Text text;
    private String words[];
    private int numWords = 0;
    private final static String EOL = System.lineSeparator();

    public DaleChall(Text text, String filename) {
        words = new String[3000];
        readList(filename);
        this.text = text;
    }

    private void readList(String filename) {
        File f = new File(filename);
        if (f.exists()) {   //It exists so read the file
            FileInputStream fistream = null;
            try {
                fistream = new FileInputStream(filename);
                try (DataInputStream in = new DataInputStream(fistream)) {
                    BufferedReader br = new BufferedReader(new InputStreamReader(in));
                    String line;
                    int counter = 0;
                    while ((line = br.readLine()) != null) {
                        words[counter] = line.trim().toLowerCase();
                        ++counter;
                    }
                    numWords = counter;
                }
            } catch (IOException ex) {
                System.out.println("Error when reading file..." + EOL + "Program will now exit");
                System.exit(0);
            } finally {
                try {
                    fistream.close();
                } catch (IOException ex) {  //Just in case...
                    System.out.println("Error when reading file..." + EOL + "Program will now exit");
                    System.exit(0);
                }
            }
        } else {
            System.out.println("File does not exist!" + EOL + "Program will now exit");
            System.exit(0);
        }
    }

    public double calcReadingEase() {
        int numSentences = text.getSentenceCount();
        int totalWords = text.getWordCount();
        String[] textWords = text.getWords();
        int wordsNotOnList = 0;
        for (String word : textWords) {
            if (!isWordOnList(word)) {
                ++wordsNotOnList;
            }
        }
        return 0.1579 * ((double) wordsNotOnList / totalWords) * 100 + 0.0496 * ((double) totalWords / numSentences) + 3.6365;
    }

    public void printGradeLevel() {
        double rawScore = calcReadingEase();
        System.out.println("Dale-Chall Raw Score = " + round(rawScore));
        if (Double.toString(rawScore).indexOf(".") == 1) {
            //Between 0.0 and 9.999
            if (rawScore < 5.0) {
                System.out.println("Grade 4 and below");
            } else {
                //2*(first digit - 5) + 5 = Lower bound of grades
                //2*(first digit -5) + 6 = Upper bound of grades
                System.out.println("Grades " + (2 * (Integer.valueOf(Double.toString(rawScore).substring(0, 1)) - 5) + 5) + "-" + (2 * (Integer.valueOf(Double.toString(rawScore).substring(0, 1)) - 5) + 6));
            }
        } else {
            System.out.println("Grades 16 and above");
        }
    }

    private boolean isWordOnList(String word) {
        return Arrays.asList(words).contains(word.toLowerCase());
    }

    private String round(double d) {
        //Added on 2/1/2013
        DecimalFormat returnable = new DecimalFormat("#.##");
        if (returnable.format(d).indexOf(".") == returnable.format(d).length() - 2) {
            return returnable.format(d) + "0";
        } else if (!returnable.format(d).contains(".")) {
            return returnable.format(d) + ".00";
        }
        return returnable.format(d);
    }
}
